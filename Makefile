TARGET=firmware
MCU=atmega328pb
SOURCES=twislave.c rc5.c light_ws2812.c main.c

PROGRAMMER= linuxspi
PORT= -P /dev/spidev0.0

#Ab hier nichts verändern
OBJECTS=$(SOURCES:.c=.o)
CFLAGS=-c -O2 -Wall -DF_CPU=8000000UL
LDFLAGS=

all: hex eeprom

hex: $(TARGET).hex

eeprom: $(TARGET)_eeprom.hex

$(TARGET).hex: $(TARGET).elf
	avr-objcopy -O ihex -j .data -j .text $(TARGET).elf $(TARGET).hex

$(TARGET)_eeprom.hex: $(TARGET).elf
	avr-objcopy -O ihex -j .eeprom --change-section-lma .eeprom=1 $(TARGET).elf $(TARGET)_eeprom.hex

$(TARGET).elf: $(OBJECTS)
	avr-gcc $(LDFLAGS) -mmcu=$(MCU) $(OBJECTS) -o $(TARGET).elf

.c.o:
	avr-gcc $(CFLAGS) -mmcu=$(MCU) $< -o $@

size:
	avr-size --mcu=$(MCU) -C $(TARGET).elf

program:
	sudo ./avrdude -C avrdude.conf -p $(MCU)  $(PORT)  $(BAUD) -c $(PROGRAMMER) -Uflash:w:$(TARGET).hex:a

fuses:
	sudo ./avrdude -C avrdude.conf -p $(MCU)  $(PORT)  $(BAUD) -c $(PROGRAMMER) -U lfuse:w:0xa2:m -U hfuse:w:0xd9:m -U efuse:w:0x07:m 

clean_tmp:
	rm -rf *.o
	rm -rf *.elf

clean:
	rm -rf *.o
	rm -rf *.elf
	rm -rf *.hex

