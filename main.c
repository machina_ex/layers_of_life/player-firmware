/* */
#include <stdint.h>

#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "light_ws2812.h"
#include "rc5.h"
#include "twislave.h"

volatile uint8_t rxbuffer[buffer_size];				//Der Empfangsbuffer, der vom Slave ausgelesen werden kann.
volatile uint8_t txbuffer[buffer_size];				//Der Sendebuffe, der vom Master ausgelesen werden kann.
volatile uint8_t buffer_adr; 						//"Adressregister" für den Buffer
volatile uint8_t isr_flag = 0;

volatile uint8_t pcint_count = 0;

int main(void)
{
  
  //this pin is low as long as the PI is running
  DDRB &= ~(1<<PB1);
  
  //Define IR input as input
  DDRE &= ~(1<<PE2);
  PORTE |= (1<<PE2);

  //Switch on general power
  DDRE |= (1<<PE3);
  PORTE |= (1<<PE3);

  //Switch on headtracker reader
  DDRC |= (1<<PC2);
  PORTC |= (1<<PC2);
  
  //Define PC0 & PC1 as inputs for SW01 and SW02
  DDRC &= ~((1<<PC0) | (1<<PC1));
  
  //Define PD7 as input for the CHARGE status, enable pullup
  DDRD &= ~(1<<PD7);
  PORTD |= (1<<PD7);


  struct cRGB led[1];
  
  led[0].r = 1; led[0].g = 0; led[0].b = 0;

  ws2812_setleds(led, 1);
  
  RC5_Init();
  
  init_twi_slave(0x42);

  uint16_t command;
  
  uint8_t ws2812_retransmit = 0;

  while(1){
    if(RC5_poll(&command)){
        txbuffer[1] = (uint8_t) (command >> 8);
        txbuffer[2] = (uint8_t) (command & 0xff); 
    }
    
    if(led[0].r != rxbuffer[0] || led[0].g != rxbuffer[1] || led[0].b != rxbuffer[2] || ws2812_retransmit){
        led[0].r = rxbuffer[0];
        led[0].g = rxbuffer[1];
        led[0].b = rxbuffer[2];
        isr_flag = 0;
        ws2812_retransmit = 0;
        ws2812_setleds(led, 1);
        if (isr_flag){
            ws2812_retransmit = 1;
        }
            
    }    
    txbuffer[0] = (PINC & (1 << PINC0)) | (PINC & (1 << PINC1)) | (PINE & (1<<PE2)) | (PIND & (1 << PIND7));
    
    if (PINB & (1 << PINB1)) {
        //Switch off general power
        PORTE &= ~(1<<PE3);
    }
    
  }
}





//*/
