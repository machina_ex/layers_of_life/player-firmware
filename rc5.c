#include "rc5.h"

#include <avr/io.h>
#include <avr/interrupt.h>

/* The formula to calculate ticks is as follows 
 * TICKS = PULSE_LENGTH / (1 / (CPU_FREQ / TIMER_PRESCALER))
 * Where CPU_FREQ is given in MHz and PULSE_LENGTH in us.
 * LONG_MIN should usually be SHORT_MAX + 1 */
#define TIMER_PRESCALER 8
#define SHORT_MIN 444  /* 444 microseconds */
#define SHORT_MAX 1333 /* 1333 microseconds */
#define LONG_MIN 1334 /* 1334 microseconds */
#define LONG_MAX 2222 /* 2222 microseconds */

typedef enum {
    STATE_START1, 
    STATE_MID1, 
    STATE_MID0, 
    STATE_START0, 
    STATE_ERROR, 
    STATE_BEGIN, 
    STATE_END
} State;

const uint8_t trans[4] = {0x01, 0x91, 0x9b, 0xfb};
uint16_t command;
uint8_t ccounter;
State state = STATE_BEGIN;


void RC5_Init()
{
    
    // Timer init
    // Timer anhalten, Prescaler Reset
    GTCCR |= (1 << TSM) | (1 << PSRASY);  
    
    TCCR3A = 0;
    
    // ICNC3 -> Noise canceler, ICES3 -> trigger at rising edge, CS31 -> prescaler 1/8 => 1/(8Mhz / 8) = 1µs per count
    TCCR3B |= (1 << ICNC3) | (1 << ICES3) | (1 << CS31);

    // ICIE3 -> Input Capture Interrupt Enable, TOIE3 -> Overflow Interrupt Enable
    // TIMSK3 |= (1<<ICIE3) | (1<<TOIE3);
    
    // TSM -> Timer/Counter Synchronization Mode Timer starten
    GTCCR &= ~(1 << TSM); 
    
    RC5_Reset();
}


inline void RC5_Reset()
{
    ccounter = 14;
    command = 0;
    state = STATE_BEGIN;
}

uint8_t RC5_poll(uint16_t *new_command) 
{
    // input capture occured if ICF3 = 1
    if (!(TIFR3 & (1 << ICF3))){
        // no input capture happend so return immediately
        return 0;
    }
    
    uint8_t has_new = 0;
    //clear input capture flag by writing ICF = 1
    TIFR3 |= (1 << ICF3);
    
    static uint16_t ref_capture = 0;
    uint16_t current_capture = ICR3;
    uint16_t delay = current_capture - ref_capture;

    /* TSOP2236 pulls the data line up, giving active low,
     * so the output is inverted. If data pin is high then the edge
     * was falling and vice versa.
     * 
     *  Event numbers:
     *  0 - short space falling
     *  2 - short pulse rising
     *  4 - long space falling
     *  6 - long pulse rising
     */
    uint8_t event = (TCCR3B & _BV(ICES3)) ? 0 : 2;
    
    //now the other Edge should be detected
    TCCR3B ^= (1 << ICES3);
    
    
    if(delay > LONG_MIN && delay < LONG_MAX)
    {
        event += 4;
    }
    else if(delay < SHORT_MIN || delay > SHORT_MAX)
    {
        /* If delay wasn't long and isn't short then
         * it is erroneous so we need to reset but
         * we don't return from interrupt so we don't
         * loose the edge currently detected. */
        RC5_Reset();
    }

    if(state == STATE_BEGIN)
    {
        ccounter--;
        command |= 1 << ccounter;
        state = STATE_MID1;
        ref_capture = current_capture;
        return has_new;
    }
    
    State newstate = (trans[state] >> event) & 0x03;

    if(newstate == state || state > STATE_START0)
    {
        /* No state change or wrong state means
         * error so reset. */
        RC5_Reset();
        return has_new;
    }
    
    state = newstate;
    
    /* Emit 0 - jest decrement bit position counter
     * cause data is already zeroed by default. */
    if(state == STATE_MID0)
    {
        ccounter--;
    }
    else if(state == STATE_MID1)
    {
        /* Emit 1 */
        ccounter--;
        command |= 1 << ccounter;
    }
    
    /* The only valid end states are MID0 and START1.
     * Mid0 is ok, but if we finish in MID1 we need to wait
     * for START1 so the last edge is consumed. */
    if(ccounter == 0 && (state == STATE_START1 || state == STATE_MID0))
    {
        state = STATE_END;
        *new_command = command;
        has_new = 1;
    }
    ref_capture = current_capture;
    
    return has_new;
}
