#!/usr/bin/env python3

import asyncio
import smbus
import time
import json
import struct
import socket
import websockets


class BatteryReader:
    def __init__(self, bus):
        self.dev = 0x36
        #self.lsb_mAh = 0.714286
        self.lsb_mAh = 0.5
        self.lsb_mV = 0.078125
        
        #self.lsb_mA = 0.002232143
        self.lsb_mA = 0.15625
        self.lsb_percent = 1/256
        self.lsb_s = 5.625
        self.bus = bus
        
        statusPOR = self.bus.read_word_data(self.dev, 0x00) & 0x002

        if statusPOR != 0:
	        while self.bus.read_word_data(self.dev,0x3d) & 1:
		        time.sleep(1)
		        
        hibCFG = self.bus.read_word_data(self.dev, 0xba)
        self.bus.write_word_data(self.dev,0x60,0x90)
        self.bus.write_word_data(self.dev,0xba,0x00)
        self.bus.write_word_data(self.dev,0x60,0x00)

        #DesignCap
        self.bus.write_word_data(self.dev,0x18,int(2600/0.714286)) #mAh
        #IChgTerm
        self.bus.write_word_data(self.dev,0x1e,int(10/0.002232143)) #mA
        #VEmpty
        self.bus.write_word_data(self.dev,0x3A,int(3000/0.078125)) # mV

        self.bus.write_word_data(self.dev,0xDB,0x8000)

        self.bus.write_word_data(self.dev,0xba,hibCFG)

    def read_battery(self):
        data = {}
        data["status"] = self.bus.read_word_data(self.dev, 0x00)
        data["repCap"] = self.bus.read_word_data(self.dev, 0x05)*self.lsb_mAh 
        data["repSOC"] = self.bus.read_word_data(self.dev, 0x06)*self.lsb_percent
        data["tte"] = self.bus.read_word_data(self.dev, 0x11)*self.lsb_s/60
        data["vcell"] = self.bus.read_word_data(self.dev, 0x09)*self.lsb_mV
        data["avgVcell"] = self.bus.read_word_data(self.dev, 0x19)*self.lsb_mV
        data["current"] = struct.unpack("<h",bytes(self.bus.read_i2c_block_data(self.dev,0x0a,2)))[0]*self.lsb_mA
        data["avgCurrent"] = struct.unpack("<h",bytes(self.bus.read_i2c_block_data(self.dev,0x0b,2)))[0]*self.lsb_mA
        return data


class MMC5983MA:

    # Register map for MMC5983MA'
    # http://www.memsic.com/userfiles/files/DataSheets/Magnetic-Sensors-Datasheets/MMC5983MA_Datasheet.pdf

    _ADDRESS = 0x30
    
    _XOUT_0 = 0x00
    _XOUT_1 = 0x01
    _YOUT_0 = 0x02
    _YOUT_1 = 0x03
    _ZOUT_0 = 0x04
    _ZOUT_1 = 0x05
    _XYZOUT_2 = 0x06
    _TOUT = 0x07
    _STATUS = 0x08
    _CONTROL_0 = 0x09
    _CONTROL_1 = 0x0A
    _CONTROL_2 = 0x0B
    _CONTROL_3 = 0x0C
    _PRODUCT_ID = 0x2F # Should be 0x30
    
    # Sample rates
    _MODR_ONESHOT = 0x00
    _MODR_1Hz = 0x01
    _MODR_10Hz = 0x02
    _MODR_20Hz = 0x03
    _MODR_50Hz = 0x04
    _MODR_100Hz = 0x05
    _MODR_200Hz = 0x06 # BW = 0x01 only
    _MODR_1000Hz = 0x07 # BW = 0x11 only

    # Bandwidths
    _MBW_100Hz = 0x00  # 8 ms measurement time
    _MBW_200Hz = 0x01  # 4 ms
    _MBW_400Hz = 0x02  # 2 ms
    _MBW_800Hz = 0x03  # 0.5 ms

    # Set/Reset as a function of measurements
    _MSET_1 = 0x00 # Set/Reset each data measurement
    _MSET_25 = 0x01 # each 25 data measurements
    _MSET_75 = 0x02
    _MSET_100 = 0x03
    _MSET_250 = 0x04
    _MSET_500 = 0x05
    _MSET_1000 = 0x06
    _MSET_2000 = 0x07

    def __init__(self, bus):
        self.bus = bus

    def get_chip_id(self):
        return self.bus.read_byte_data(self._ADDRESS, self._PRODUCT_ID)

    def reset(self):
        # Set bit 7 to 1 to reset MMC5983MA
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_1,0x80)
        time.sleep(0.01) # Wait 10 ms for all registers to reset 

    def init(self, modr, mbw):
        # enable auto set/reset (bit 5 == 1)
        # this set/reset is a low current sensor offset measurement for normal use
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x20);  
 
        # set magnetometer bandwidth
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_1, mbw)  

        # enable continuous measurement mode (bit 3 == 1), set sample rate
        # enable automatic Set/Reset (bit 7 == 1), set set/reset rate
        # this set/reset is a high-current "deGaussing" that should be used only to recover from 
        # high magnetic field detuning of the magnetoresistive film
        # self.bus.write_byte_data(self._ADDRESS, self.CONTROL_2, 0x80 | (MSET << 4) | 0x08 | MODR);  
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_2, 0x08 | modr)


    def self_test(self):
        # clear control registers
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x00)
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_1, 0x00) 
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_2, 0x00)

        self.SET() # enable set current
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x01)  #enable one-time mag measurement
        time.sleep(0.01)

        set_x, set_y, set_z = struct.unpack(">HHH",bytes(self.bus.read_i2c_block_data(self._ADDRESS, self._XOUT_0, 6)))

        self.RESET(); # enable reset current
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x01);  #enable one-time mag measurement
        time.sleep(0.01)

        reset_x, reset_y, reset_z = struct.unpack(">HHH",bytes(self.bus.read_i2c_block_data(self._ADDRESS, self._XOUT_0, 6)))

        print (f"x-axis self test = {abs(set_x-reset_x)}, should be >100")
        print (f"y-axis self test = {abs(set_y-reset_y)}, should be >100")
        print (f"z-axis self test = {abs(set_z-reset_z)}, should be >100")



    def get_offset(self):
        self.power_down();
 
        self.SET(); # enable set current
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x01);  #enable one-time mag measurement
        time.sleep(0.01) 

        set_x, set_y, set_z = struct.unpack(">HHH",bytes(self.bus.read_i2c_block_data(self._ADDRESS, self._XOUT_0, 6)))

        self.RESET(); # enable reset current
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x01);  #enable one-time mag measurement
        time.sleep(0.01)

        reset_x, reset_y, reset_z = struct.unpack(">HHH",bytes(self.bus.read_i2c_block_data(self._ADDRESS, self._XOUT_0, 6)))


        x = (set_x+reset_x)/2;
        y = (set_y+reset_z)/2;
        z = (set_z+reset_y)/2;
        
        return (x,y,z)



    def SET(self):
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x08)  
        time.sleep(0.001) # self clearing after 500 us



    def RESET(self):
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_0, 0x10)
        time.sleep(0.001) # self clearing after 500 us



    def status():
        # Read status register
        return self.bus.read_byte_data(self._ADDRESS, self._STATUS) 


    def clear_int():
        # Clear data ready interrupts
        temp = self.bus.read_byte_data(self._ADDRESS, self._STATUS)  
        self.bus.write_byte_data(self._ADDRESS, self._STATUS, temp & 0x01)   



    def read_data():
        self.bus.read_i2c_block_data(self._ADDRESS, self._XOUT_0, 7);  # Read the 7 raw data registers into data array
        dest_x = (rawData[0] << 10 | rawData[1] << 2 | (rawData[6] & 0xC0) >> 6) # Turn the 18 bits into a unsigned 32-bit value
        dest_y = (rawData[2] << 10 | rawData[3] << 2 | (rawData[6] & 0x30) >> 4) # Turn the 18 bits into a unsigned 32-bit value
        dest_z = (rawData[4] << 10 | rawData[5] << 2 | (rawData[6] & 0x0C) >> 2) # Turn the 18 bits into a unsigned 32-bit value
        return (dest_x, dest_y, dest_z)

    def read_temperature():
        return self.bus.read_byte_data(self._ADDRESS, self._TOUT);  # Read the raw temperature register 

    def power_down(self):
        temp = self.bus.read_byte_data(self._ADDRESS, self._CONTROL_2) # read register contents
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_2, temp & ~(0x07)); # clear lowest four bits
        delay(20); # make sure to finish the lest measurement

    def power_up(self, modr):
        temp = self.bus.read_byte_data(self._ADDRESS, self._CONTROL_2) # read register contents
        self.bus.write_byte_data(self._ADDRESS, self._CONTROL_2, temp | modr)  # start continuous mode



class LoLPlayerInterface:
    def __init__(self, bus):
        self.dev = 0x42
        self.bus = bus
        self.ir = 0
        self.status = 0
        self.ws_connections = []


    def poll(self):
        status, msb, lsb = self.bus.read_i2c_block_data(self.dev, 0, 3)
        self.status = status
        self.ir = (msb * 256 + lsb)

    def get_system_status(self):
        sys_status = {"button0":(self.status & 0x01) != 0, "button1": (self.status & 0x02) != 0, "charging": (self.status & 0x80) == 0}
        return sys_status
        
    def get_ir(self):
        return self.ir
    
    def write_led(self, r, g, b):
        self.bus.write_i2c_block_data(self.dev, 0, [r, b, g])


    async def run_ws_server(self, host, port):
        async with websockets.serve(self._register_ws_connection, host, port):
            await asyncio.Future()  # run forever
            
    async def _register_ws_connection(self, websocket, _):
        self.ws_connections.append(websocket)
        async for message in websocket:
            print (f"received {message}")
            msg = json.loads(message)
            if "topic" in msg and f"{socket.gethostname()}/shutdown" in msg["topic"] and msg["message"] == "1":
                print ("shutting down because of shutdown message")
                await asyncio.sleep(5)
                await asyncio.create_subprocess_shell("sudo shutdown -h now")
            
    async def _write_to_ws(self, data):
        print (data)
        for conn in self.ws_connections:
            try:
                await conn.send(data)
            except websockets.exceptions.ConnectionClosed:
                self.ws_connections.remove(conn)

    async def publish_to_ws(self, topic, data):
        await self._write_to_ws(json.dumps({"topic":topic,"message":data}))


async def main():
    bus = smbus.SMBus(1)
    battreader = BatteryReader(bus)
    player_interface = LoLPlayerInterface(bus)
    asyncio.ensure_future(player_interface.run_ws_server("0.0.0.0", 8967))
    #player_interface.write_led(255,0,0)
    
    #magnetometer = MMC5983MA(bus)
    #print (magnetometer.get_chip_id())
    
    #magnetometer.init(MMC5983MA._MODR_50Hz, MMC5983MA._MBW_100Hz)
    
    #magnetometer.self_test()


    batt_status = battreader.read_battery()
    sys_status = player_interface.get_system_status()
    ir_status = player_interface.get_ir()
    last_battery_time = time.time()
    
    

    while True:

        if last_battery_time < time.time() - 5:
            last_battery_time = time.time()
            batt = battreader.read_battery()
            if batt != batt_status:
                batt_status = batt
                await player_interface.publish_to_ws(f"{socket.gethostname()}/battery",batt_status)

        player_interface.poll()
        sys = player_interface.get_system_status()
        if sys != sys_status:
            sys_status = sys
            await player_interface.publish_to_ws(f"{socket.gethostname()}/system",sys_status)
            if sys_status["charging"]:
                print ("shutting down because the charger is plugged in")
                await asyncio.sleep(5)
                await asyncio.create_subprocess_shell("sudo shutdown -h now")


        ir = player_interface.get_ir()
        if ir != ir_status:
            ir_status = ir
            await player_interface.publish_to_ws(f"{socket.gethostname()}/zone",ir)
        await asyncio.sleep(1/25)

if __name__ == "__main__":
    asyncio.run(main(), debug=True)

